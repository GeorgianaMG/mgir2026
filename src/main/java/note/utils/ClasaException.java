package note.utils;

/**
 * Created by Geo on 3/20/2018.
 */
public class ClasaException extends Exception {
    public ClasaException(String message) {
        super(message);
    }
}
