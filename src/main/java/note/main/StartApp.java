package note.main;

import note.controller.NoteController;
import note.model.Corigent;
import note.model.Medie;
import note.model.Nota;
import note.repository.NoteRepositoryMock;
import note.utils.ClasaException;

import java.io.*;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

//functionalitati
//i.	 adaugarea unei note la o anumita materie (nr. matricol, materie, nota acordata); 
//ii.	 calcularea mediilor semestriale pentru fiecare elev (nume, nr. matricol), 
//iii.	 afisarea elevilor coringenti, ordonati descrescator dupa numarul de materii la care nu au promovat şi alfabetic dupa nume.


public class StartApp {

	/**
	 * @param args
	 * @throws ClasaException
	 */
	public static void main(String[] args) throws ClasaException {
		// TODO Auto-generated method stub
		NoteController ctrl = new NoteController();
		List<Medie> medii = new LinkedList<Medie>();
        NoteRepositoryMock mock =new NoteRepositoryMock();
		List<Corigent> corigenti = new ArrayList<Corigent>();
		ctrl.readElevi(args[0]);
		ctrl.readNote(args[1]);
		ctrl.creeazaClasa(ctrl.getElevi(), ctrl.getNote());
		String filename = "note.txt";
		boolean gasit = false;
		while(!gasit) {
			System.out.println("1. Adaugare Nota");

			System.out.println("2. Calculeaza medii");
			System.out.println("3. Elevi corigenti");
			System.out.println("4. Iesire");
			BufferedReader br = new BufferedReader(new InputStreamReader(System.in) );
			boolean seAdaugaNota = false;
		    try {
				int option = Integer.parseInt(br.readLine());
				switch(option) {
//				case 1:adaugaNota();
					case 1:
						System.out.print("Elevi:\n");
					BufferedReader in = new BufferedReader(new FileReader("C:\\Uni\\An3\\VVSS\\04-ProiectNoteElevi\\ProiectNoteElevi\\src\\main\\elevi.txt"));
						String text = in.readLine();
						System.out.println(text);
						while (in.ready()) {
							text = in.readLine();
							System.out.println(text);
						}
						in.close();


						System.out.print("Materii:\n");
						in = new BufferedReader(new FileReader("C:\\Uni\\An3\\VVSS\\04-ProiectNoteElevi\\ProiectNoteElevi\\src\\main\\note.txt"));
						String materii = in.readLine();
//						System.out.println(materii);
						while (in.ready()) {
							materii = in.readLine();
							System.out.println(materii);
						}
						in.close();


						System.out.print("Da nota:\n");
						System.out.print("Nr matricol:\n");
						int nrmatricol = Integer.parseInt(br.readLine());
						System.out.print("Materie:\n");
						String materie = (br.readLine());
						System.out.print("Nota:\n");
						int nota = Integer.parseInt(br.readLine());
                        Object note;
                        note = new Nota(nrmatricol, materie , nota );
                        if(!mock.validareNota((Nota) note)) {
                            return;
                        }
						writeResultToFile("C:\\Uni\\An3\\VVSS\\04-ProiectNoteElevi\\ProiectNoteElevi\\src\\main\\note.txt", "\n" +nrmatricol +";" + materie +";"+ nota );
						break;
					// Afiseaza elevii

				case 2: medii = ctrl.calculeazaMedii();
						for(Medie medie:medii)
							System.out.println(medie);
						break;
				case 3: corigenti = ctrl.getCorigenti();
						for(Corigent corigent:corigenti)
							System.out.println(corigent);
						break;
				case 4: gasit = true;
						break;
				default: System.out.println("Introduceti o optiune valida!");
				}
				
			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	private static void writeResultToFile(String filename, String result){
		try {
			BufferedWriter bw = new BufferedWriter(new FileWriter(filename, true));
			bw.write(result);
			bw.flush();
		}catch(IOException ex){
			ex.printStackTrace();
		}

	}


	}



